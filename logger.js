//
// Copyright (c) 2016 by Usercentrics GmbH . All Rights Reserved.
// created by iurii.
//


var os = require('os');
var winston = require('winston');

/* jshint -W116 */
var NOMETA = process.env.NOMETA == 1;
var NOSTACK = process.env.NOSTACK == 1;
var APPNAME = process.env.LOG_APPNAME || '';
/* jshint +W116 */


var logger = new winston.Logger({
  levels: {
    fatal: 4,
    error: 3,
    info: 2,
    warn: 1,
    debug: 0
  }
});

winston.config.addColors({
  fatal: 'blue',
  error: 'red',
  info: 'green',
  warn: 'yellow',
  debug: 'grey'
});

logger.add(winston.transports.Console, { level: 'debug' });
logger.transports.console.colorize = true;

module.exports = {
  add: logger.add.bind(logger),
  remove: logger.remove.bind(logger),
  getLogger: getLogger,
};

function getLogger(opts) {
  var module = opts.module;
  var loggerInstance = {};
  Object.keys(logger.levels).forEach(function (level) {
    loggerInstance['' + level + ''] = log.bind(null, module, level);
    loggerInstance['' + level + 'AndRethrow'] = logAndRethrow(module, level);
    loggerInstance['' + level + 'AndPassthrough'] = logAndPassthrough(module, level);
  });
  loggerInstance.logAndRethrow = logAndRethrow(module, 'error');
  loggerInstance.logAndPassthrough = logAndPassthrough(module, 'debug');
  return loggerInstance;
}

function logAndRethrow(module, level) {
  return function (msg) {
    return function (err) {
      log(module, level, msg, err);
      throw err;
    };
  };
}

function logAndPassthrough(module, level) {
  return function (msg) {
    return function (res) {
      log(module, level, msg, res);
      return res;
    };
  };
}

var id = 0;
var host = os.hostname();
function log(module, level, message, metadata) {
  logger.log(level, NOMETA ? {
    id: id,
    timestamp: Date.now() / 1000,
    module: module || '',
    message: message,
    metadata: processMetadata(metadata || ''),
  } : {
    id: id,
    app: APPNAME,
    host: host,
    timestamp: new Date(),
    module: module || '',
    level: level,
    message: message,
    metadata: metadata || '',
  });
  return id++;
}

function processMetadata(metadata) {
  if (metadata instanceof Error)
    metadata = NOSTACK ? metadata.message : metadata.stack;
  if (typeof metadata === 'string')
    return metadata;
  try {
    return JSON.stringify(metadata, null, 2);
  }
  catch (err) {
    return metadata;
  }
}
