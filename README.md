# hug❤it logger

Hugit logger class.


## Install

```
$ npm install --save-dev hugit-logger
```


## Usage

```bash
LOG_APPNAME=starship node starship-app.js
```

```js
var log = require('hugit-logger').getLogger({module: 'mega blaster'});
log.debug('pew pew');
log.info('collision in 3');
log.warn('collision in 2');
log.error('PANIC!!!11 Argh');
log.fatal('hit!', {damage: '42hp'});

return new Promise(function armBomb(resolve, reject) {
  reject(new Error('Peace cannot be kept by force; ' +
                   'it can only be achieved by understanding.'))
})
.catch(log.logAndRethrow('malfunction'))
.then(log.warnAndPassthrough('unexpected'));
```


## License

MIT © [Iurii Proshchenko](https://github.com/spect)
