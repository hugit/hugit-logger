//
// Copyright (c) 2016 by Usercentrics GmbH . All Rights Reserved.
// created by iurii.
//

export interface ILoggerFactory {
  getLogger({module: string}): ILogger
}

export interface ILogger {
  fatal(...args: any[]): void;
  error(...args: any[]): void;
  info(...args: any[]): void;
  warn(...args: any[]): void;
  debug(...args: any[]): void;
  log(...args: any[]): void;
  fatalAndRetrow(...args: any[]): IFunc<void>;
  errorAndRetrow(...args: any[]): IFunc<void>;
  infoAndRetrow(...args: any[]): IFunc<void>;
  warnAndRetrow(...args: any[]): IFunc<void>;
  debugAndRetrow(...args: any[]): IFunc<void>;
  logAndRetrow(...args: any[]): IFunc<void>;
  fatalAndPassthrough<T>(...args: any[]): IFunc<T>;
  errorAndPassthrough<T>(...args: any[]): IFunc<T>;
  infoAndPassthrough<T>(...args: any[]): IFunc<T>;
  warnAndPassthrough<T>(...args: any[]): IFunc<T>;
  debugAndPassthrough<T>(...args: any[]): IFunc<T>;
  logAndPassthrough<T>(...args: any[]): IFunc<T>;
}

export interface IFunc<T> {
  (...args: any[]): T
}
